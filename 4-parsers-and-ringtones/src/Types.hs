{-# LANGUAGE TypeApplications #-}

module Types where

import Data.Int (Int32)

type Pulse = [Float]
type Seconds = Float
type Samples = Float
type Hz = Float
type Semitones = Float
type Beats = Float
type Ringtone = String

data Tone = C | CSharp | D | DSharp | E | F | FSharp | G | GSharp | A | ASharp | B deriving (Enum, Eq, Show)
data Octave = Zero | One | Two | Three | Four | Five | Six | Seven | Eight deriving (Enum, Eq, Show)
data Duration = Full | Half | Quarter | Eighth | Sixteenth | Thirtysecond | Dotted Duration deriving (Eq, Show)
data Note = Pause Duration | Note Tone Octave Duration deriving (Eq, Show)

-- Done: Schrijf en documenteer zipWithL, die zo ver mogelijk zipt;
-- als na het zippen, nog waarden in de eerste lijst zitten, plakt het deze er achteraan;
-- als na het zippen, nog waarden in de tweede lijst zitten, worden die weggegooid.
{- | Zipping two list of variable length, and retaining the remainder of the left list
1. input: func: function, l1: list1, l2: list2
2. Create a new list using zipWith using func l1 and l2, this list is as long as the shortest given list
3. Create a second new list by dropping as many values in l1 as l2 holds, if l2 is longer or equal in length to l1 we'll be left with no list, if l1 is longer than l2 then all excess elements will remain in this list
4. Return: Coconate results of 2. and 3. and return this result as answer

__Examples:__
@
zipWithL (+) [1..4] [10..14] = [11 14 16 18]
zipWithL (+) [1..5] [10..14] = [11 14 16 18 5]
zipWithL (+) [1..4] [10..15] = [11 14 16 18]
@

-}
zipWithL :: (a -> b -> a) -> [a] -> [b] -> [a]
zipWithL func l1 l2 = zipWith func l1 l2 ++ drop (length l2) l1

-- Done: Schrijf en documenteer zipWithR, die zo ver mogelijk zipt;
-- als na het zippen, nog waarden in de eerste lijst zitten, worden die weggegooid;
-- als na het zippen, nog waarden in de tweede lijst zitten, plakt het deze er achteraan.
{- | Zipping two list of variable length, and retaining the remainder of the right list
1. input: func: function, l1: list1, l2: list2
2. Create a new list using zipWith using func l1 and l2, this list is as long as the shortest given list
3. Create a second new list by dropping as many values in l2 as l1 holds, if l1 is longer or equal in length to l2 we'll be left with no list, if l2 is longer than l1 then all excess elements will remain in this list
4. Return: Coconate results of 2. and 3. and return this result as answer

__Examples:__
@
zipWithR (+) [1..4] [10..14] = [11 14 16 18]
zipWithR (+) [1..5] [10..14] = [11 14 16 18]
zipWithR (+) [1..4] [10..15] = [11 14 16 18 15]
@

-}
zipWithR :: (a -> b -> b) -> [a] -> [b] -> [b]
zipWithR func l1 l2 = zipWith func l1 l2 ++ drop (length l1) l2

data Sound = FloatFrames [Float]
  deriving Show

floatSound :: [Float] -> Sound
floatSound = FloatFrames

instance Eq Sound where
  (FloatFrames xs) == (FloatFrames ys) = (all ((<  0.001) . abs) $ zipWith (-) xs ys) && (length xs == length ys)

-- Done: Schrijf de instance-declaraties van Semigroup en Monoid voor Sound.
-- Semigroup is een typeclass met een operator (<>), die twee waarden combineert;
-- in deze context betekent dat "twee geluiden na elkaar afspelen".
-- Monoid bouwt voort op Semigroup, maar heeft een mempty; een lege waarde.
instance Semigroup Sound where
  (FloatFrames a) <> (FloatFrames b) = FloatFrames (a++b)

instance Monoid Sound where
  mempty = FloatFrames []

-- Done: Schrijf en documenteer de operator `(<+>)` die twee `Sound`s  tot een enkel `Sound` combineert.
-- Combineren betekent hier: de geluiden tegelijkertijd afspelen. 
-- Als de lijsten niet even lang zijn, moet wat er overblijft achteraan worden toegevoegd!
{-|An operator for Sound, we sum the two sounds together, no sound is lost if one Sound is longer than the other.
1. input: Sound1: FloatFrame ,Sound2: FloatFrame
2. test: number of sounds in Sound1 equal to number of sounds in Sound2, if yes then we sum them with zipWith (+) x y else:
3. test: number of sounds in Sound1 greater to number of sounds in Sound2, if yes, then we sum them using zipWithL (+) x y else:
4. we assume the number of sounds in Sound2 is greater than the number of sounds in Sound1 and we sum them using zipWithR (+) x y
5. return: A summed new Sound made by adding Sound1 and Sound2 together.

__Examples:__
@
FloatFrames [0.0, 0.2, 0.1, 0.3, 0.2, 0.4] <+> FloatFrames [0.5, 0.4, 0.3, 0.2, 0.1, 0.0] = FloatFrames [0.4999, 0.5999, 0.3999, 0.4999, 0.2999, 0.3999]
@
-}
(<+>) :: Sound -> Sound -> Sound
(FloatFrames x) <+> (FloatFrames y)
  | length x == length y = FloatFrames (zipWith (+) x y)
  | length x > length y = FloatFrames (zipWithL (+) x y)
  | otherwise = FloatFrames (zipWithR (+) x y)

floatToInt32 :: Float -> Int32
floatToInt32 x = fromIntegral $ round x

getAsInts :: Sound -> [Int32]
getAsInts (FloatFrames fs) = map (floatToInt32 . \x -> x * fromIntegral (div (maxBound @Int32 ) 2 )) fs

type Track = (Instrument, [Note])

newtype Instrument = Instrument (Hz -> Seconds -> Pulse)

instrument :: (Hz -> Seconds -> Pulse) -> Instrument
instrument = Instrument
-- 

newtype Modifier = Modifier (Pulse -> Pulse)
          
modifier :: (Pulse -> Pulse) -> Modifier  -- :: (Pulse = [Float] -> Pulse = [Float])
modifier = Modifier

instance Semigroup Modifier where
  (Modifier m1) <> (Modifier m2) = Modifier $ m1 . m2

-- TODO: Schrijf en documenteer de functie modifyInstrument, die een Modifier met een Instrument combineert. 
-- TIPS: Kijk goed naar de types! Gebruik een lambda om een functie te maken, die je verpakt in een Instrument.
{-|

-}
modifyInstrument :: Instrument -> Modifier -> Instrument -- :: 
modifyInstrument instrument modifier = undefined
-- modifyInstrument (Instrument pulse) (Modifier a) = instrument(a pulse)
-- map (\y -> modifier) c

-- TODO: Schrijf en documenteer de functie arrange die de functie in het meegegeven Instrument toepast op de frequentie en duur. 
-- TIPS: Kijk goed naar de types!
arrange :: Instrument -> Hz -> Seconds -> Sound
arrange = undefined
