{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: decision trees
    Copyright   : (c) Nick Roumimper, 2021
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum schrijven we een algoritme om de decision tree te bouwen voor een gegeven dataset.
    Meer informatie over het principe achter decision trees is te vinden in de stof van zowel DEP als CM.
    Let op dat we hier naar een simpele implementatie van decision trees toewerken; parameters zoals maximale
    diepte laten we hier expres weg. Deze code blijft splitsen tot er geen verbetering meer mogelijk is.
    De resulterende boom zal dus "overfit" zijn, maar dat is de verwachting.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib where

import Data.List (group, sort, partition)

-- Allereerst definiëren we een datatype voor één enkele rij uit onze traindataset. (CRecord, voor Classification Record.)
-- Een rij uit onze dataset bestaat uit 
--     1) een lijst van numerieke eigenschappen van onze meeteenheid (properties);
--     2) een label voor de klasse waar deze meeteenheid toe behoort (label).
-- Bij het bouwen van onze boom weten we ook het label; ons doel is voor nieuwe data om op basis van de properties
-- te voorspellen welk label erbij hoort - maar dat implementeren we pas helemaal aan het eind.

data CRecord = CRecord { properties :: [Float]
                       , label :: String
                       }
  deriving (Show, Eq)

-- Onze dataset (CDataset, voor Classification Dataset) is dus simpelweg een lijst van CRecords.

type CDataset = [CRecord]

-- Bijgevoegd een paar simpele datasets, die je kunt gebruiken om mee te testen.

miniSet1 :: CDataset
miniSet1 = [CRecord [1,1] "blue", CRecord [2,2] "green", CRecord [3,3] "pink", CRecord [4,4] "purple", CRecord [5,5] "gray"]

miniSet2 :: CDataset
miniSet2 = [CRecord [1,1] "pink", CRecord [2,2] "pink", CRecord [3,3] "purple", CRecord [4,4] "blue", CRecord [5,5] "blue"]

miniSet3 :: CDataset
miniSet3 = [CRecord [1,1] "blue", CRecord [1,2] "green", CRecord [2,1] "green", CRecord [2,2] "green", CRecord [3,1] "orange", CRecord [3,2] "orange"]

-- ..:: Sectie 1: Het bepalen van de Gini impurity ::..
-- De Gini impurity meet of de dataset rijen bevat uit maar één klasse ("puur"),
--                       of veel rijen uit allerlei verschillende klassen ("impuur").
-- Dit getal zit tussen de 0 en de 1, waar 0 zo puur mogelijk is en 1 zo impuur mogelijk.
-- Als we een splitsing maken in onze boom, willen we de Gini impurity zo laag mogelijk krijgen.

-- Bij het bepalen van de Gini impurity kijken we alleen naar de labels van de data.
-- Done: schrijf en becommentarieer de functie getLabels die een lijst van alle labels in een dataset teruggeeft.
{- |Acquires and returns the labels from a CDataset, as a list of labels
1. Calling label on a CRecord returns the associated label, with map we apply this to each CRecord and create a list in the process.
2. Return: the list of labels.

__Examples:__
@
getLabels [CRecord [1,1] "blue", CRecord [2,2] "green", CRecord [3,3] "pink"] = ["blue","green","pink"]
@
-}
getLabels :: CDataset -> [String]
getLabels  = map label

-- Om de Gini impurity te bepalen, willen we weten hoe vaak alle labels voorkomen.
-- Voorbeeld: ["a", "b", "a", "c", "c", "a"] wordt [("a", 3), ("b", 1), ("c", 2)]
-- We hebben de volgende twee hulpfuncties geïmporteerd:
--     group :: Eq a => [a] -> [[a]]
--        ^ zet alle gelijke waarden naast elkaar in een eigen lijst.
--          Voorbeeld: [1,1,2,2,2,1] => [[1,1],[2,2,2],[1]] 
--     sort :: Ord a => [a] -> [a]
--        ^ sorteert de lijst.
-- Done: schrijf en becommentarieer de functie countLabels die telt hoe vaak alle labels in de dataset voorkomen.
{- |Determines for a CDataset how often each label occurs
1. With getLabels, sort and group we create a sorted list Grouped of lists, where each element is a list of labels of the same name.
2. With the use of map we apply a lambda function \y to each element of the Grouped list, creating a new Result list where each element is of the form: ("label",quantity)
3. Return: A sorted list with all the labels and their frequency, sorted alphabeticly.

__Examples:__
@
countLabels [CRecord [1,1] "blue", CRecord [1,2] "green", CRecord [2,1] "green"] = [("blue",1),("green",2)]
@
-}
countLabels :: CDataset -> [(String, Int)]
countLabels set = sort (map (\y -> (head y, length y)) (group $ sort $ getLabels set))

-- Voor toekomstig gebruik willen we alvast een functie hebben die het meest voorkomende label
-- van een dataset geeft. Bij gelijkspel mag je eender welk teruggeven.
-- TODO: schrijf en becommentarieer de functie mostFrequentLabel op basis van countLabels.
-- HINT: gebruik een functie uit de Prelude. We gebruiken mostFrequentLabel pas in de laatste sectie!
{- |Determines which label is the most frequent occurance in a CDataset, if two labels occur equally often, it will return the alphabeitlcly lowest one. 
1. We create a list of all the labels using countLabels 
2. Then we grab the 2nd value each time with map, returning a list of all the frequencies
3. Finally we grab the 

__Examples:__
@
mostFrequentLabel [CRecord [1,1] "blue", CRecord [1,2] "green", CRecord [2,1] "green"] = "green"
mostFrequentLabel [CRecord [1,1] "blue", CRecord [2,2] "green", CRecord [3,3], "pink"] = "blue"
@
-}
mostFrequentLabel :: CDataset -> String
mostFrequentLabel set = fst $ countLabels set !!index
    where   labels = map snd $ countLabels set                              -- De lijst van voorkomend aantal. [("Groen",1),("Pink",2),..] -> [1,2,..]
            index = head $ filter ((== maximum labels) . (labels !!)) [0..] -- De (eerste) grootste index uit de lijst van labels.

-- We definiëren de volgende hulpfunctie (fd, voor "Float Division") om twee Ints te delen als twee Floats.
-- Voorbeeld: fd 3 4 ~> 0.75 (een Float), i.p.v. 0 (een Int).
fd :: Int -> Int -> Float
fd x y = (/) (fromIntegral x) (fromIntegral y)

-- De Gini impurity van één dataset is:
--     de som van de kansen voor elke klasse dat
--         ik, uit alle rijen, willekeurig een rij uit die klasse trek én
--         ik, uit alle rijen, willekeurig een rij uit een andere klasse trek.
-- Zie ook de stof van CM en de Canvas van DEP voor meer context.
-- Done: schrijf en becommentarieer de functie gini die de Gini impurity van één dataset bepaalt.
{- |Calculates the gini impurity of a CDataset
1. When we have k classes, the probability of a sample belonging to classs i denotes to p, which gives Gini impurity D = 1-sum(p1^2-...pi^2)
2. We determine the p for each class by (size of p / total size of CDataset) i.e. if we have 4 records, 3 of which are blue, then p_blue = 0.75 and p_rest = 0.25
3. To do this automaticly, 1st we use countLabels to get all the labels and count their frequency
4. 2nd we use map to acquire all the numbers, as the frequency of each label is the second value in the countLabels list, and save this list as labels
5. 3rd we sum the result of 2nd, and store this in a variable lengte for later
6. 4th we use a map to apply lambda \y to each element of our labels list, the lambda \y does lengte * (1- value / lengte) for each element in labels, creating a list
7. Return: float: the sumation of the list created at step 4, which is the gini impurity for this CDataset

__Examples:__
@
gini [CRecord [1,1] "blue", CRecord [2,2] "green", CRecord [3,3] "pink", CRecord [4,4] "purple", CRecord [5,5] "gray"] = 0.80
@
-}
gini :: CDataset -> Float
gini set = sum $ map (\y -> fd y lengte*(1-fd y lengte)) labels
    where
        labels = map snd $ countLabels set
        lengte = sum labels

-- De gecombineerde Gini impurity van twee datasets (in ons geval: na een splitsing)
-- is de gewogen som van de Gini impurity van beide sets.
-- Voorbeeld: mijn splitsing leidt tot
--     1) een dataset met 3 rijen en een Gini impurity van 0.2;
--     2) een dataset met 2 rijen en een Gini impurity van 0.1.
-- Dan is de gecombineerde Gini impurity (0.2 * (3/5)) + (0.1 * (2/5)) = 0.16.
-- Done: schrijf en becommentarieer de functie giniAfterSplit die de gecombineerde Gini impurity van twee datasets bepaalt.
{- |Calculates the total gini impurity of two CDatasets
1. We use gini to calculate the gini impurity of each set then we multiply this by (set length / total length), where the total length is equal to the size of set1 + set2
2. Then we add the results of each set from 1. together and acquire the gini impurity total
3. Return: float: total gini impurity of both CDatasets

__Examples:__
@
giniAfterSplit [CRecord [1,1] "blue", CRecord [2,2] "green"] [CRecord [3,3] "pink"] = 0.33
@
-}
giniAfterSplit :: CDataset -> CDataset -> Float
giniAfterSplit set1 set2 = (gini set1 * fd (length set1) lengte) + (gini set2 * fd (length set2) lengte)
    where
        lengte = length set1 + length set2 -- De gecombineerde lengte van set1 en set2 


-- ..:: Sectie 2 - Het genereren van alle mogelijke splitsingen in de dataset ::..
-- Bij het genereren van onze decision tree kiezen we telkens de best mogelijke splitsing in de data.
-- In deze simpele implementatie doen we dat brute force: we genereren alle mogelijke splitsingen
-- in de data, en checken ze allemaal. Hier beginnen we door de splitsingen te genereren.

-- We slaan elke mogelijke splitsing op in het datatype CSplit (voor Classification Split). Deze bestaat uit:
--     1) de eigenschap waarop gesplitst wordt, opgeslagen als de index in de lijst van properties (feature);
--     2) de waarde van deze feature waarop we splitsen - ofwel kleiner-gelijk-aan, ofwel groter dan (value).
-- Let op: feature refereert aan de positie in de lijst van properties van een CRecord.
-- Oftewel: als we het hebben over feature 1 van CRecord [8.0, 5.0, 3.0] "x", bedoelen we 5.0.
data CSplit = CSplit { feature :: Int
                     , value :: Float
                     }
    deriving (Show, Eq, Ord)

split1 :: CSplit 
split1 = CSplit {feature = 0, value = 1.5}


-- Allereerst willen we alle waarden van een bepaalde feature in een aparte lijst hebben.
-- TODO: schrijf en becommentarieer de functie getFeature, die gegeven een feature (index in de lijst properties) en een dataset,
--       een lijst teruggeeft van alle waarden van die feature.
{- |Retrieves all the feature values at index int and returns this as a list of values
1. We use properties to get the properties from the CDataset as a list,
2. With map and the index, we then get the desired feature from each property

__Examples:__
@
getFeature 1 [CRecord [0,1] "blue", CRecord [1,2] "green", CRecord [2,3] "pink"] = [1.0,2.0,3.0}
getFeature 0 [CRecord [0,1] "blue", CRecord [1,2] "green", CRecord [2,3] "pink"] = [0.0,1.0,2.0}
@
-}
getFeature :: Int -> CDataset -> [Float]
getFeature index  = map ((!!index) . properties)

-- Als we een lijst van waarden hebben, hoeven we alleen naar de unieke waarden te kijken.
-- Tegelijkertijd is het wel zo makkelijk als de unieke waarden alvast zijn gesorteerd.
-- TODO: schrijf en becommentarieer de functie getUniqueValuesSorted, die uit een lijst van Floats de unieke waarden gesorteerd teruggeeft.
-- HINT: gebruik de hulpfuncties uit de vorige sectie.
{- |

__Examples:__
@
getUniqueValuesSorted 
@
-}
getUniqueValuesSorted :: [Float] -> [Float]
getUniqueValuesSorted set = map head $ group $ sort set

-- Als we de dataset splitsen, doen we er verstandig aan om niet precies op een waarde uit de dataset te splitsen.
-- In plaats daarvan splitsen we op het gemiddelde van alle twee naast elkaar gelegen waarden.
-- Voorbeeld: getAverageValues [2.0, 3.0, 5.0, 9.0] ~> [2.5, 4.0, 7.0]
-- Voor de traindata maakt dat geen verschil, maar voor het voorspellen van nieuwe waarden wel.
-- TODO: schrijf en becommentarieer de functie getAverageValues, die de gemiddelden bepaalt van alle paren van twee waarden in een lijst.
{- |Calculates the average of two values provided by a list
1. We take the average of x and the head of xs and add this value to the list
2. Then we recursively call the function again with xs, thus getting the pairswise average of each pair. 

__Examples:__
@
getAverageValues 
@
-}
getAverageValues :: [Float] -> [Float]
getAverageValues [a] = []
getAverageValues (x:xs) = [(x + head xs) / 2.0] ++ getAverageValues xs

-- Met deze functies kunnen we alle mogelijke CSplits bepalen voor één gegeven feature.
-- Voorbeeld: een dataset met in feature 2 de waarden [9.0, 2.0, 5.0, 3.0] wordt
--            [CSplit 2 2.5, CSplit 2 4.0, CSplit 7.0].
-- TODO: schrijf en becommentarieer de functie getFeatureSplits, die alle mogelijke CSplits bepaalt voor een gegeven feature.
{- |

__Examples:__
@
getFeatureSplits 0 [CRecord [1,1] "blue", CRecord [2,2] "green", CRecord [3,3] "pink", CRecord [4,4] "purple", CRecord [5,5] "gray"] =
[CSplit {feature = 0, value = 1.5},CSplit {feature = 0, value = 2.5},CSplit {feature = 0, value = 3.5},CSplit {feature = 0, value = 4.5}]
@
-}
getFeatureSplits :: Int -> CDataset -> [CSplit]
getFeatureSplits feature set = map (\y -> CSplit feature y) (getAverageValues $ getUniqueValuesSorted $ getFeature feature set)

-- Door getFeatureSplits toe te passen voor alle mogelijke features, kunnen we alle mogelijke CSplits bepalen.
-- Done: schrijf en becommentarieer de functie getAllFeatureSplits, die alle mogelijke CSplits van een dataset bepaalt.
{- |Determines all possible CSsplits for a given CDataset
1. We create a list features of [0..n-1], where n is the size of the Datasets properties
2. With map we create a list of lists, using each value in features to make sure each possible featureSplit gets generated regardless of the number of features it has
3. we concat the list of lists so we only return one list
4. Return: All possible CSplits in a single list, sorted by feature position number

__Examples:__
@
getAllfeatureSplits [CRecord [1,1] "blue", CRecord [2,2] "green", CRecord [3,3] "pink", CRecord [4,4] "purple", CRecord [5,5] "gray"] =
[CSplit {feature = 0, value = 1.5},CSplit {feature = 0, value = 2.5},CSplit {feature = 0, value = 3.5},CSplit {feature = 0, value = 4.5}
,CSplit {feature = 1, value = 1.5},CSplit {feature = 1, value = 2.5},CSplit {feature = 1, value = 3.5},CSplit {feature = 1, value = 4.5}]
@
-}
getAllFeatureSplits :: CDataset -> [CSplit]
getAllFeatureSplits set = concat $ map (\y -> getFeatureSplits y set) [0..(length $ properties $ set !! 0) -1]

-- ..:: Sectie 3 - Het vinden van de beste splitsing ::..
-- Nu we alle splitsingen hebben gegenereerd, rest ons nog de taak de best mogelijke te vinden.
-- Hiervoor moeten we eerst de functies schrijven om één CDataset, op basis van een CSplit,
-- te splitsen in twee CDatasets.

-- Allereerst schrijven we de functie waarmee we bepalen in welke dataset een CRecord belandt
-- gegeven een bepaalde splitsing. Deze functie moet True teruggeven als de waarde van de feature  
-- kleiner-gelijk-aan is aan de splitswaarde, en False als deze groter is dan de splitswaarde.
-- Voorbeelden: gegeven CSplit 1 3.0 en CRecord [4.0, 2.0, 9.0] "x", is het resultaat True.
--              gegeven CSplit 1 1.0 en CRecord [4.0, 2.0, 9.0] "x", is het resultaat False.
-- Done: schrijf en becommentarieer de functie splitSingleRecord, die voor een enkel CRecord True of False teruggeeft.
{- |Determines whether the value of a feature is smaller or equal to the splitting value
1. Feature from CSplit gives us the index at which we are considering our split, value CSplit gives us the number against which to consider
2. With properties on CRecord we get the properties list, using !! feature CSplit we find the value of that index position and compare it against the number
3. Return: False if the CRecord property value is greater than the splitting number, True otherwise

__Examples:__
@
splitSingleRecord [Csplit 1 3.0] [CRecord [1.0, 2.0, 3.0] "x"] = False
splitSingleRecord [Csplit 1 1.0] [CRecord [1.0, 2.0, 3.0] "x"] = True
@
-}
splitSingleRecord :: CSplit -> CRecord -> Bool
splitSingleRecord split record = (properties record !! feature split) <= value split

-- Nu kunnen we de functie schrijven die één dataset opsplitst in twee, op basis van een CSplit object.
-- Done: schrijf en becommentarieer de functie splitOnFeature, die één dataset opsplitst in twee.
-- HINT: gebruik een functie uit de Prelude. Onthoud dat CDataset = [CRecord]!
{- |Splits a CDataset into two new CDatasets, using a CSplit feature
1. We use filter twice, once for each list we want to return, applying it over the given Dataset, with the condition of lambda \y, for the first filter we want to meet the condition and for the second filter we do not want to
2. We select by comparing the value , against the feature property, here we acquire our value and feature from CSplit, and we use feature to select whether we want to compare the value against the 1st or 2nd feautre of our property
3. If this comparison is larger than the property value, then filter will stick it in the left list, if it is larger or equal, it will put it in the right list
4. Return: 2 CDatasets split on their record values

__Examples:__
@
splitOnFeature [0, 1.5] [CRecord [1,1] "blue", CRecord [1,2] "green", CRecord [2,1] "green", CRecord [2,2] "green", CRecord [3,1] "orange", CRecord [3,2] "orange"] = ([CRecord {properties = [1.0,1.0], label = "blue"},CRecord {properties = [1.0,2.0], label = "green"}],[CRecord {properties = [2.0,1.0], label = "green"},CRecord {properties = [2.0,2.0], label = "green"},CRecord {properties = [3.0,1.0], label = "orange"},CRecord {properties = [3.0,2.0], label = "orange"}])
@
-}
splitOnFeature :: CDataset -> CSplit -> (CDataset, CDataset)
splitOnFeature dataset split = (filter (\y -> value split > properties y !! feature split) dataset, filter (\y -> value split <= properties y !! feature split) dataset) 

-- Nu kunnen we:
--     1) alle splitsingen genereren voor een CDataset, met behulp van Sectie 2;
--     2) de datasets die resulteren bij elk van die splitsingen genereren.
-- Wel is het van belang dat we onthouden welke splitsing bij welke twee datasets hoort.
-- TODO: schrijf en becommentarieer de functie generateAllSplits, die voor een gegeven dataset alle mogelijke splitsingen "uitprobeert".
{- |Generates all possible splits for a given dataset, returns a tuple containing all possible split sets.
1. We use getAllFeatureSplits to acquire a list of all possible splits.
2. 

__Examples:__
@
generateAllSplits [CRecord [1,1] "blue", CRecord [2,2] "green", CRecord [3,3] "pink", CRecord [4,4] "purple", CRecord [5,5] "gray"] = 
[(CSplit {feature = 0, value = 1.5},[CRecord {properties = [1.0,1.0], label = "blue"}],[CRecord {properties = [2.0,2.0], label = "green"},CRecord {properties = [3.0,3.0], label = "pink"},CRecord {properties = [4.0,4.0], label = "purple"},CRecord {properties = [5.0,5.0], label = "gray"}]),
(CSplit {feature = 0, value = 2.5},[CRecord {properties = [1.0,1.0], label = "blue"},CRecord {properties = [2.0,2.0], label = "green"}],[CRecord {properties = [3.0,3.0], label = "pink"},CRecord {properties = [4.0,4.0], label = "purple"},CRecord {properties = [5.0,5.0], label = "gray"}]),
(CSplit {feature = 0, value = 3.5},[CRecord {properties = [1.0,1.0], label = "blue"},CRecord {properties = [2.0,2.0], label = "green"},CRecord {properties = [3.0,3.0], label = "pink"}],[CRecord {properties = [4.0,4.0], label = "purple"},CRecord {properties = [5.0,5.0], label = "gray"}]),
(CSplit {feature = 0, value = 4.5},[CRecord {properties = [1.0,1.0], label = "blue"},CRecord {properties = [2.0,2.0], label = "green"},CRecord {properties = [3.0,3.0], label = "pink"},CRecord {properties = [4.0,4.0], label = "purple"}],[CRecord {properties = [5.0,5.0], label = "gray"}]),
(CSplit {feature = 1, value = 1.5},[CRecord {properties = [1.0,1.0], label = "blue"}],[CRecord {properties = [2.0,2.0], label = "green"},CRecord {properties = [3.0,3.0], label = "pink"},CRecord {properties = [4.0,4.0], label = "purple"},CRecord {properties = [5.0,5.0], label = "gray"}]),
(CSplit {feature = 1, value = 2.5},[CRecord {properties = [1.0,1.0], label = "blue"},CRecord {properties = [2.0,2.0], label = "green"}],[CRecord {properties = [3.0,3.0], label = "pink"},CRecord {properties = [4.0,4.0], label = "purple"},CRecord {properties = [5.0,5.0], label = "gray"}]),
(CSplit {feature = 1, value = 3.5},[CRecord {properties = [1.0,1.0], label = "blue"},CRecord {properties = [2.0,2.0], label = "green"},CRecord {properties = [3.0,3.0], label = "pink"}],[CRecord {properties = [4.0,4.0], label = "purple"},CRecord {properties = [5.0,5.0], label = "gray"}]),
(CSplit {feature = 1, value = 4.5},[CRecord {properties = [1.0,1.0], label = "blue"},CRecord {properties = [2.0,2.0], label = "green"},CRecord {properties = [3.0,3.0], label = "pink"},CRecord {properties = [4.0,4.0], label = "purple"}],[CRecord {properties = [5.0,5.0], label = "gray"}])]@
-}
generateAllSplits :: CDataset -> [(CSplit, CDataset, CDataset)]
generateAllSplits dataset = map (\y -> (y, filter (\x -> value y > properties x !! feature y) dataset, filter (\x -> value y <= properties x !! feature y) dataset)) (getAllFeatureSplits dataset)


--Helper functions, to extract information from the unique datatype generated by allSplits
getFst :: (CSplit, CDataset, CDataset) -> CSplit
getFst (x,_,_) = x

getSnd :: (CSplit, CDataset, CDataset) -> CDataset
getSnd (_,x,_) = x

getTrd :: (CSplit, CDataset, CDataset) -> CDataset
getTrd (_,_,x) = x


--Get all feature splits with getAllFeatureSplits dataset (List! = y)
--Use lambda on that list(\y-> splitOnFeature dataset y) produces CDataset, Cdataset

-- De laatste stap van deze sectie combineert Sectie 1 en Sectie 3:
--     1) Genereer alle mogelijke splits;
--     2) Bepaal welke van deze splitsingen het beste resultaat geeft - oftewel, de laagste Gini impurity.
-- Hierbij willen we graag zowel de Gini impurity als de splitsing zelf onthouden.
-- TODO: schrijf en becommentarieer de functie findBestSplit, die voor een dataset de best mogelijke splitsing vindt.
-- HINT: gebruik een functie uit de Prelude. Hoe werkt "kleiner dan" voor tupels?
{- |Finds the split with the lowest giny impurity for a given CDataset
1. 
1. : 
    Get all the feature splits
    From this, get A: select only the CDataset, don't care about the CSplit
    Run A through Gini with map to get B
    Sort B 
2. Problem:
    We have CDataset, we'll get (CDataset, CDataset) we want (Float, Csplit) where Float is the Gini Impurity score, and Cspli the found split. For some reason I thought we wanted something wildly more complicated....
    T1: Can probably get the desired answer by running it through a lambda function, as then I can grab the datasets, and wedge in the Csplit.
    T2: Will have to remember to keep track of what split is best's position so I can reunite the CSplit. Either by keeping it all in one tuple object, or by sorting myself so I can keep track of positions.

__Examples:__
@

@
-}

findBestSplit :: CDataset -> (Float, CSplit)
findBestSplit dataset = sort (map (\y -> (giniAfterSplit (getSnd y) (getTrd y), getFst y)) (generateAllSplits dataset)) !! 0

-- ..:: Sectie 4 - Genereren van de decision tree en voorspellen ::..
-- In deze laatste sectie combineren we alle voorgaande om de decision tree op te bouwen,
-- en deze te gebruiken voor voorspellingen.

-- We introduceren het datatype van onze boom, de DTree (Decision Tree).
-- In de DTree is sprake van twee opties:
--     1) We hebben een blad van de boom bereikt, waarin we een voorspelling doen van het label (Leaf String);
--     2) We splitsen op een bepaalde eigenschap, met twee sub-bomen voor <= en > (Branch CSplit DTree DTree).
-- Zoals je al ziet is de definitie van Branch CSplit DTree DTree recursief; er kan dus een onbepaald aantal
-- vertakkingen zijn, maar uiteindelijk eindigt elke vertakking in een blad (Leaf).
-- Let op: we onthouden niet de records uit de dataset, maar wel waarop we ze gesplitst hebben (CSplit)!
data DTree = Branch CSplit DTree DTree | Leaf String deriving (Show, Eq, Ord)

-- De logica achter het recursief bouwen van een decision tree is als volgt:
--     ALS de Gini impurity van de dataset 0.0 is (perfect gesplitst)
--     OF de Gini impurity wordt zelfs met de best mogelijke splitsing niet beter
--         DAN geef ik een Leaf terug met daarin het vaakst voorkomende label;
--     ZO NIET,
--         DAN geef ik een Branch terug met daarin de best mogelijke splitsing
--         en de decision trees (sub-bomen) op basis van de twee datasets na die splitsing.
-- Done: schrijf en becommentarieer de functie buildDecisionTree.
{-|Builds a decision tree out of a given CDataset
1. We check the gini impurity of the given CDataset first, 
2. If Gini is 0, or equal to the best possible gini for this set, we assign it as a Leaf, with the most frequent label as the prediction
3. Otherwise, we create a Branch, by filing in the CSplit and then calling the function recursively again
4. Note that Branch = CSplit DTree DTree 
5. Return: A decision tree fit for this CDataset

__Examples:__
@
buildDecisionTree [CRecord [1,1] "blue", CRecord [1,2] "green", CRecord [2,1] "green", CRecord [2,2] "green", CRecord [3,1] "orange", CRecord [3,2] "orange"] =
      Branch (CSplit {feature = 0, value = 2.5}) (Branch (CSplit {feature = 0, value = 1.5}) (Branch (CSplit {feature = 1, value = 1.5}) (Leaf "blue") (Leaf "green")) (Leaf "green")) (Leaf "orange")  
@
-}
buildDecisionTree :: CDataset -> DTree
buildDecisionTree set = 
    if gini set == 0 || gini set == fst (findBestSplit set) then Leaf $ mostFrequentLabel set
    else Branch bestSplit (buildDecisionTree (fst splitSet)) (buildDecisionTree (snd splitSet))
    where 
        bestSplit = snd $ findBestSplit set         --CSplit
        splitSet = splitOnFeature set bestSplit     --(CDataset, CDataset)
      

-- Tot slot, bij het voorspellen weten we alleen de eigenschappen ([Float]), niet het label.
-- Done: schrijf en becommentarieer de functie predict, die op basis van een boom en de gegeven eigenschappen het label voorspelt.
{-|Makes a prediction on the label given a DTree and a [Float].
1. A DTree can be either a Leaf or a Branch, if we get a Leaf, we return the associated label immediately, as there's no more decisions to be made.
2. A Branch we test, if the check value is smaller or equal to the value held in the CSplit, we assume that the left path of the branch is to be gone down, otherwise we go down the right path.
3. We go down a path by simply calling the function again, but now one level deeper.
3. The above steps continue untill we reach the end in a Leaf.
4. Return: A label associated with the given value.

__Examples:__
predict Branch (CSplit {feature = 0, value = 2.5}) (Branch (CSplit {feature = 0, value = 1.5}) (Branch (CSplit {feature = 1, value = 1.5}) (Leaf "blue") (Leaf "green")) (Leaf "green")) (Leaf "orange") [1.0] = "blue"
predict Branch (CSplit {feature = 0, value = 2.5}) (Branch (CSplit {feature = 0, value = 1.5}) (Branch (CSplit {feature = 1, value = 1.5}) (Leaf "blue") (Leaf "green")) (Leaf "green")) (Leaf "orange") [2.5] = "green"
-}
predict :: DTree -> [Float] -> String
predict (Leaf(label)) _ = label
predict (Branch (CSplit feature value) l r) check
    | check <= [value] = predict l check
    | otherwise = predict r check