{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: recursie en lijsten
    Copyright   : (c) Brian van de Bijl, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum oefenen we met het schrijven van simpele functies in Haskell.
    Specifiek leren we hoe je recursie en pattern matching kunt gebruiken om een functie op te bouwen.
    LET OP: Hoewel al deze functies makkelijker kunnen worden geschreven met hogere-orde functies,
    is het hier nog niet de bedoeling om die te gebruiken.
    Hogere-orde functies behandelen we verderop in het vak; voor alle volgende practica mag je deze
    wel gebruiken.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- Done: Schrijf en documenteer de functie ex1, die de som van een lijst getallen berekent.
-- Voorbeeld: ex1 [3,1,4,1,5] ~> 14
{- |Calculates the sum of all items in the provided list recursively

1. return 0 if the given list contains no elements
2. using recursive calls, add up all elements in the list, effectively this will add the last element to the second last element going from the back to the front
3. return resulting sum

__Examples:__
@
ex1 [] = 0
ex1 [1,2,3,4] = 10
@
-}
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (x:xs) = x + ex1 xs

-- Done: Schrijf en documenteer de functie ex2, die alle elementen van een lijst met 1 ophoogt.
-- Voorbeeld: ex2 [3,1,4,1,5] ~> [4,2,5,2,6]
{- |Increases each element in the input list by +1 recursively

1. return an empty list if an empty list was given
2. we take the head of the list, x and add +1 
3. we attempt to call the function again with the remainder of the list xs and add the result of 2 as an element to the list in front of it
4. the recursion ends once we try it with an empty list, and we'll have ended up with the result, the functio neffectivley runs from right to left but list order is preserved
3. return a list with all elements increased by +1

__Examples:__
@
ex2 [] = []
ex2 [1,2,3] = [2,3,4]
@
-}
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (x:xs) = x+1 : ex2 xs

-- Done: Schrijf en documenteer de functie ex3, die alle elementen van een lijst met -1 vermenigvuldigt.
-- Voorbeeld: ex3 [3,1,4,1,5] ~> [-3,-1,-4,-1,-5]
{- |Multiplies each value in a given list by -1 recursively
1. return an empty list if an empty list was given
2. using recursive calls, multiply each head element by -1, and then concatenate result with the next call of the function using the remainder of the list
3. return a list with all elements multiplied by -1

__Examples:__
@
ex3 [] = []
ex3 [1,2,3] = [-1,-2,-3]
@
-}
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = x * (-1) : ex3 xs

-- Done: Schrijf en documenteer de functie ex4, die twee lijsten aan elkaar plakt.
-- Voorbeeld: ex4 [3,1,4] [1,5] ~> [3,1,4,1,5]
-- Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. 
-- Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
{- |concatenate two intger lists into one integer list recursively
1. if we get an empty list 1 return list 2
2. using recursive calls, we take the head of list 1 and add this to the front of the recursive call of ex4
3. ex4 will keep running untill it runs out of list1
4. return the resulting concatenated list

__Examples:__
@
ex4 [] [1,2,3] = [1,2,3]
ex4 [1,2,3] [4,5,6] = [1,2,3,4,5,6]
@
-}
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] y = y
ex4 (x:xs) y = x : ex4 xs y

-- Done: Schrijf en documenteer een functie, ex5, die twee lijsten van gelijke lengte paarsgewijs bij elkaar optelt.
-- Voorbeeld: ex5 [3,1,4] [1,5,9] ~> [4,6,13]
{- |Adds list 1  to list 2, pair-wise 
1. if both lists are empty, return an empty list
2. take the head of list 1 and list 2, add them together
3. place the result of 2 in front of the list created by calling ex5 again with the remainder of list 1 and list 2 repeat till lists are empty
4. return the result of list 1 pairswise added with list 2

__Examples:__
@
ex5 [] [] = []
ex5 [1,2,3] [6,6,5] = [7,7,7]
@
-}
ex5 :: [Int] -> [Int] -> [Int]
ex5 [][] = []
ex5 (x:xs) (y:ys) = x + y : ex5 xs ys

-- Done: Schrijf en documenteer een functie, ex6, die twee lijsten van gelijke lengte paarsgewijs met elkaar vermenigvuldigt.
-- Voorbeeld: ex6 [3,1,4] [1,5,9] ~> [3,5,36] 
{- |Multiplies list 1 with list 2, pair-wise
1. if both lists are emtpy, return an empty list
2. take the head of list 1 and list2, multiply them with each other
3. place the result of 2 in front of the list created by calling ex6 again with the remainder of list 1 and list 2 repeat till lists are empty
4. return the result of list 1 multiplied pair wise with list 2

__Examples:__
@
ex6 [][] = []
ex6 [1,2,3] [1,2,3] = [1,4,9]
@
-}
ex6 :: [Int] -> [Int] -> [Int]
ex6 [][] = []
ex6 (x:xs) (y:ys) = x * y : ex6 xs ys

-- Done: Schrijf en documenteer een functie, ex7, die de functies ex1 en ex6 combineert tot een functie die het inwendig product uitrekent.
-- Voorbeeld: ex7 [3,1,4] [1,5,9] geeft 3*1 + 1*5 + 4*9 = 44 terug als resultaat.
{- |Calculates the dot product of list 1 and list 2
1. we use ex6 to calculate the pairs wise multiplication of list 1 and list 2
2. we use ex1 to calculate the sum of the resulting list from 1
3. return dot product of list 1 and list 2

__Examples:__
@
ex7 [1,2,3] [4,5,6] = 32
@
-}
ex7 :: [Int] -> [Int] -> Int
ex7 x y = ex1 $ ex6 x y