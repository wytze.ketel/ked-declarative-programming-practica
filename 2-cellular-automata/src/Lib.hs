{-|
    Module      : Lib
    Description : Checkpoint voor V2DeP: cellulaire automata
    Copyright   : (c) Brian van der Bijl & Nick Roumimper, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum gaan we aan de slag met 1D cellulaire automata.
    Een cellulair automaton is een rekensysteem dat, gegeven een startsituatie en regels,
    in tijdstappen bepaalt of diens cellen (vakjes) "aan" of "uit" zijn ("levend" of "dood"). 
    Denk aan Conway's Game of Life [<https://playgameoflife.com/>], maar dan in één dimensie (op een lijn).
    Als we de tijdstappen verticaal onder elkaar plotten, krijgen we piramides met soms verrassend complexe patronen erin.
    LET OP: lees sowieso de informatie op [<https://mathworld.wolfram.com/Rule30.html>] en de aangesloten pagina's!
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib where

import Data.Maybe (catMaybes) -- Gebruikt, kwam van pas.
import Data.List (unfoldr)
import Data.Tuple (swap)


-- ..:: Sectie 1: Basisoperaties op de FocusList ::..

-- Om de state van een cellulair automaton bij te houden bouwen we eerst een set functies rond een `FocusList` type. 
-- Dit type representeert een (1-dimensionale) lijst, met een enkel element dat "in focus" is. 
-- Het is hierdoor mogelijk snel en makkelijk een enkele cel en de cellen eromheen te bereiken.
-- Een voorbeeld van zo'n "gefocuste" lijst: 
--     [0, 1, 2, <3>, 4, 5]
-- waarbij 3 het getal is dat in focus is.
-- In Haskell representeren we de bovenstaande lijst als:
--     FocusList [3,4,5] [2,1,0]
-- waarbij het eerste element van de eerste lijst in focus is.
-- De elementen die normaal vóór de focus staan, staan in omgekeerde volgorde in de tweede lijst.
-- Hierdoor kunnen we makkelijk (lees: zonder veel iteraties te hoeven doen) bij de elementen rondom de focus,
-- en de focus makkelijk één plaats opschuiven.

-- als focus ligt op 0 ([<0>, 1, 2, 3, 4, 5])dan
--    FocusList [0, 1, 2, 3, 4, 5] [] 
-- als resultaat
-- FocusList geeft altijd 2 lijsten, maar de tweede lijst kan dus gewoon leeg zijn.

data FocusList a = FocusList { forward :: [a]
                             , backward :: [a]
                             }
  deriving (Show, Eq)

-- De instance-declaraties mag je voor nu negeren.
instance Functor FocusList where
  fmap = mapFocusList

-- Enkele voorbeelden om je functies mee te testen:
intVoorbeeld :: FocusList Int
intVoorbeeld = FocusList [3,4,5] [2,1,0]

stringVoorbeeld :: FocusList String
stringVoorbeeld = FocusList ["3","4","5"] ["2","1","0"]

lStrTest :: FocusList String
lStrTest = FocusList ["0","1","2","3","4","5"] []

rStrTest :: FocusList String
rStrTest = FocusList [] ["5","4","3","2","1","0"]

-- Done: Schrijf en documenteer de functie toList, die een focus-list omzet in een gewone lijst. 
-- Het resultaat bevat geen focus-informatie meer, maar moet wel op de juiste volgorde staan.
-- Voorbeeld: toList intVoorbeeld ~> [0,1,2,3,4,5]
{-
Neem 1 FocusList, in bestaande uit fw en bw.
1. bw "backward", staat fout om, met reverse bw wordt deze omgedraaid.
2. fw "forwards" staat goed om geen handeling nodig.
3. resultaat van 1. wordt met ++ gecocateneerd aan 2. 
Return: Een non FocusList List.
-}
toList :: FocusList a -> [a]
toList (FocusList fw bw) = reverse bw ++ fw

-- Done: Schrijf en documenteer de functie fromList, die een gewone lijst omzet in een focus-list. 
-- Omdat een gewone lijst geen focus heeft moeten we deze kiezen; dit is altijd het eerste element.
{-| Converts a list into a FocusList
1. we get a list x
2. We put the list as the first argument in FocusList, with an empty second list thus creating a FocusList
3. Return: FocusList made from input List
-}
fromList :: [a] -> FocusList a
fromList x = FocusList x []

-- Deze functie, die je van ons cadeau krijgt, schuift de focus één naar links op.
-- Voorbeeld: goLeft $ FocusList [3,4,5] [2,1,0] ~> FocusList [2,3,4,5] [1,0]
goLeft :: FocusList a -> FocusList a
goLeft (FocusList fw (f:bw)) = FocusList (f:fw) bw

-- Done: Schrijf en documenteer de functie goRight, die de focuslist een plaats naar rechts opschuift.
{-|Schuift een Focus in een FocusList een plaats naar rechts
Krijgen binnen 1 FocusList als (f:fw) bw.
We nemen het hoofd van de forward list, f, en plakken die als hoofd aan bij bw, vervolgens maken we een nieuwe FocusList aan met het restant fw en de nieuwe (f:bw)
Return 1 FocusList, met de focus 1 naar rechts opgeschoven.
-}
goRight :: FocusList a -> FocusList a
goRight (FocusList (f:fw) bw) = FocusList fw (f:bw)

-- Todo: Schrijf en documenteer de functie leftMost, die de focus helemaal naar links opschuift.
{-|Schuift een Focus in een FocusList een plaats naar links
Neem 1 FocusList, a
run deze door toList heen, we krijgen dan een lijst die weer gesorteerd is, deze gebruiken we dan met FocusList om een nieuwe focuslist aan te maken.
Return 1 FocusList met element 0 als focus.
-}
leftMost :: FocusList a -> FocusList a
leftMost a = FocusList (toList a) []

-- Done: Schrijf en documenteer de functie rightMost, die de focus helemaal naar rechts opschuift.
{-|Schuift een Focus van een FocusList helemaal naar rechts
Neem 1 FocusList, a
We maken van de input lijst a een gesorteerde normale lijst met gebruik van toList, deze lijst reversen we dan zodat we hem achsterstevoren hebben
vervolgens nemen we van deze lijst het hoofd (onze rightmost value) en gebruiken het hoofd en de rest van de lijst om een nieuwe FocusList aan te maken.
Return 1 FocusList, met het laatste element als focus.
-}
rightMost :: FocusList a -> FocusList a
rightMost a = FocusList [x] xs
  where x:xs = reverse (toList a)


-- Onze functies goLeft en goRight gaan er impliciet van uit dat er links respectievelijk rechts een waarde gedefinieerd is. 
-- De aanroep `goLeft $ fromList [1,2,3]` zal echter crashen, omdat er in een lege lijst gezocht wordt: er is niets verder naar links. 
-- Dit is voor onze toepassing niet handig, omdat we vaak de cellen direct links en rechts van de focus 
-- nodig hebben, ook als die (nog) niet bestaan.

-- Schrijf de functies totalLeft en totalRight die de focus naar links respectievelijk rechts opschuift; 
-- als er links/rechts geen vakje meer is, dan wordt een lege (dode) cel teruggeven. 
-- Hiervoor gebruik je de waarde `mempty`, waar we met een later college nog op in zullen gaan. 
-- Kort gezegd zorgt dit ervoor dat de FocusList ook op andere types blijft werken - 
-- je kan dit testen door totalLeft/totalRight herhaaldelijk op de `voorbeeldString` aan te roepen, 
-- waar een leeg vakje een lege string zal zijn.
-- NOTE: deze functie werkt niet met intVoorbeeld! (Omdat mempty niet bestaat voor Ints - maar daar komen we nog op terug!)

-- Grafisch voorbeeld: [⟨░⟩, ▓, ▓, ▓, ▓, ░]  ⤚totalLeft→ [⟨░⟩, ░, ▓, ▓, ▓, ▓, ░]

-- Done: Schrijf en documenteer de functie totalLeft, zoals hierboven beschreven.
{-|Schuift de focus
Neem 1 FocusList, fw bw
1. Check of de lengte van bw kleiner gelijk is aan 1
1.1 Return: FocusList mempty:bw ++ fw, we voegen een dode cel vooran bw toe, en plakken bw weer vooraan aan fw.
2. Otherwise: we nemen de eerste waarde van head, en plakken deze vooraan bij fw toe, dit is onze nieuwe Focus, de rest van bw wordt dan de tweede lijst voor de FocusList
-}
totalLeft :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalLeft (FocusList fw bw)
  | length  bw <= 1 = FocusList (mempty:bw ++ fw) []
  | otherwise = FocusList (head bw : fw) (tail bw)

-- Done: Schrijf en documenteer de functie totalRight, zoals hierboven beschreven.
{-
Neem 1 FocusList, x:fw x:bw
1. Controle of fw == [], als dit waar is gaat de focus naar een mempty toe, we gaan ervan uit dat we nooit een compleet lege FocusList binnen krijgen.
1.1 Return: FocusList [mempty] en de eenige waarde x vooraan bij bw toegevoegd.
2. Alle andere situaties zitten er nog waarden in fw, dus schuiven we de focus netjes op naar rechts.
2.1 Return: FocusList fw met waarde x vooraan bij bw toegevoegd. 

-}
totalRight :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalRight (FocusList (x:fw) bw)
  | null fw = FocusList [mempty] (x:bw)
  | otherwise = FocusList fw (x:bw)

-- ..:: Sectie 2 - Hogere-ordefuncties voor de FocusList ::..

-- In de colleges hebben we kennis gemaakt met een aantal hogere-orde functies zoals `map`, `zipWith` en `fold[r/l]`. 
-- Hier stellen we equivalente functies voor de FocusList op.

-- Done: Schrijf en documenteer de functie mapFocusList.
-- Deze werkt zoals je zou verwachten: de functie wordt op ieder element toegepast, voor, op en na de focus. 
-- Je mag hier gewoon map voor gebruiken.

{- 
Neem 1 functie func en 1 focuslist (fw bw)
Return: Focuslist fw en bw, waarbij de functie func met map op de lijsten fw en bw is toegepast.
-}
mapFocusList :: (a -> b) -> FocusList a -> FocusList b
mapFocusList func (FocusList fw bw) = FocusList (map func fw) (map func bw)

-- Done: Schrijf en documenteer de functie zipFocusListWith.
-- Deze functie zorgt ervoor dat ieder paar elementen uit de FocusLists als volgt met elkaar gecombineerd wordt:

-- [1, 2, ⟨3⟩,  4, 5]        invoer 1
-- [  -1, ⟨1⟩, -1, 1, -1]    invoer 2
--------------------------- (*)
-- [  -2, ⟨3⟩, -4, 5    ]    resultaat

-- of, in code: zipFocusListWith (*) (FocusList [3,4,5] [2,1]) (FocusList [1,-1,1,-1] [-1]) ~> FocusList [3,-4,5] [-2]
-- Oftewel: de meegegeven functie wordt aangeroepen op de twee focus-elementen, met als resultaat het nieuwe focus-element. 
-- Daarnaast wordt de functie paarsgewijs naar links/rechts doorgevoerd, waarbij gestopt wordt zodra een van beide uiteinden leeg is. 
-- Dit laatste is net als bij de gewone zipWith, die je hier ook voor mag gebruiken.

{- 
Neem 1 functie func en 2 focuslists (fw1 bw1) en (fw2 bw2)
1. Gebruik Zipwith met func fw* bw* om twee nieuwe lijsten te maken, gebruik deze lijsten met FocusList om een nieuwe focusList terug te geven. 
Return 1 FocusList waarvan de twee lijsten bewerkt zijn met de gewenste functie.
-}
zipFocusListWith :: (a -> b -> c) -> FocusList a -> FocusList b -> FocusList c
zipFocusListWith func (FocusList fw1 bw1) (FocusList fw2 bw2) = FocusList (zipWith func fw1 fw2) (zipWith func bw1 bw2)

-- TODO: Schrijf en documenteer de functie foldFocusList.
-- Het folden van een FocusList vergt de meeste toelichting: waar we met een normale lijst met een left fold en een 
-- right fold te maken hebben, folden we hier vanuit de focus naar buiten.
-- Vanuit de focus worden de elementen van rechts steeds gecombineerd tot een nieuw element, 
-- vanuit het element voor de focus gebeurt hetzelfde vanuit links. 
-- De twee resultaten van beide sublijsten (begin tot aan focus, focus tot en met eind) worden vervolgens nog een keer met de meegegeven functie gecombineerd. 
-- Hieronder een paar voorbeelden:

-- foldFocusList (*) [0, 1, 2, ⟨3⟩, 4, 5] = (0 * (1 * 2)) * ((3 * 4) * 5)
--                                       = (0 * 2) * (12 * 5)
--                                       = 0 * 60
--                                       = 0

-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = (0 - (1 - 2)) - ((3 - 4) - 5)
--                                       = (0 - (-1)) - ((-1) - 5)
--                                       = 1 - (-6)
--                                       = 7

-- Je kunt, buiten de testsuite, `testFold` uitvoeren in "stack ghci" om je functie te testen.
-- Let op: de tweede lijst van de FocusList kan leeg zijn! (De eerste technisch gezien ook, maar dan heb je geen geldige FocusList.)
-- Let op: de testcode van deze functie is opgesplitst in twee delen! Beide delen moeten slagen als de functie klopt!
-- FocusList [0, 1, 2, ⟨3⟩, 4, 5] -> [3,4,5] [2,1,0]

{-
Neem een fuctie func en een focusList fw bw
1. als bw leeg is, dan passen we foldr1 toe op de lijst fw, met als functie func.
1.1 Return: het resultaat van foldr1.
2. anders bepalen we het resultaat voor bw en fw, bw draaien we om omdat er anders problemen onstaan met specifiek strings.
2.1 Return: het resultaat van func toegepast op fw en bw.
foldr1(+) [0,1,2,3] -> 0 + (1 + (2 + 3)) 
foldl1(+) [0,1,2,3] -> ((0 + 1) + 2) + 3
-}
foldFocusList :: (a -> a -> a) -> FocusList a -> a
foldFocusList func (FocusList fw bw)
  | null bw = foldr1 func fw
  | otherwise = func (foldr1 func (reverse bw)) (foldl1 func fw)

-- foldFocusList func (FocusList (f:fw) (b:bw)) = foldr func (foldl func f fw) [(foldl func b bw)]

-- Mocht je foldFocusList handmatig willen testen, dan kun je testFold opvragen via GHCi.
-- Deze geeft True als alles klopt, en False zo niet. Uiteraard kun je ook handmatig onderdelen hier uit halen.
testFold :: Bool
testFold = and [ foldFocusList (+) intVoorbeeld               == 15
               , foldFocusList (-) intVoorbeeld               == 7
               , foldFocusList (*) (FocusList [1,3,5,7,9] []) == 945
               , foldFocusList (++) stringVoorbeeld           == "012345"
               ]


-- ..:: Sectie 2.5: Types voor cellulaire automata ::..

-- Nu we een redelijk complete FocusList hebben, kunnen we deze gaan gebruiken om cellulaire automata in te ontwikkelen.
-- In deze sectie hoef je niets aan te passen, maar je moet deze wel even doornemen voor de volgende opgaven.

-- Een cel kan ofwel levend, ofwel dood zijn.
data Cell = Alive | Dead deriving (Show, Eq)

-- De onderstaande instance-declaraties mag je in dit practicum negeren.
instance Semigroup Cell where
  Dead <> x = x
  Alive <> x = Alive

instance Monoid Cell where
  mempty = Dead

-- De huidige status van ons cellulair automaton beschrijven we als een FocusList van Cells.
-- Dit type noemen we Automaton.
type Automaton = FocusList Cell

-- De standaard starttoestand bestaat uit één levende cel in focus.
start :: Automaton
start = FocusList [Alive] []

-- De context van een cel is de waarde van een cel, samen met de linker- en rechterwaarde, op volgorde.
-- Voorbeeld: de context van 4 in [1,2,3,4,5,6] is [3,4,5].
-- In de praktijk bestaat de context altijd uit drie cellen, maar dat zie je niet terug in dit type.
type Context = [Cell]

-- Een regel is een mapping van elke mogelijke context naar de "volgende state" - levend of dood.
-- Omdat er 2^3 = 8 mogelijke contexts zijn, zijn er 2^8 = 256 geldige regels.
-- Zie voor een voorbeeld [<https://mathworld.wolfram.com/Rule30.html>].
type Rule = Context -> Cell


-- ..:: Sectie 3: Rule30 en helperfuncties ::..

-- Done: Schrijf en documenteer de functie safeHead, die het eerste item van een lijst geeft; 
-- als de lijst leeg is, wordt een meegegeven defaultwaarde teruggegeven.
{-
Neem 1 default waarde backup, en een lijst lst
1. Als lst leeg is return backup
2. Anders return de eerste waarde uit lst
-}
safeHead :: a        -- ^ Defaultwaarde
         -> [a]      -- ^ Bronlijst
         -> a
safeHead backup lst
  | null lst = backup
  | otherwise = head lst

-- Done: Schrijf en documenteer de functie takeAtLeast, die werkt als `take`, maar met een extra argument. 
-- Als de lijst lang genoeg is werkt de functie hetzelfde als `take` en worden de eerste `n` elementen teruggegeven.
-- Zo niet, dan worden zoveel mogelijk elementen teruggegeven, en wordt daarna tot aangevuld met de meegegeven defaultwaarde.
-- Voorbeelden: takeAtLeast 4 0 [1,2,3,4,5] ~> [1,2,3,4]
--              takeAtLeast 4 0 [1,2]       ~> [1,2,0,0]
{-
Neem n als gewenste aantal elementen om te pakken, backup als de default waarde en een lijst lst als invoer.
Return: de eerste n waarden van lst, samengevoegd met de rest waarde lengte van backup, als de restwaarde negatief of 0 is dan voegen we een lege lijst toe aan de eerste lijst en dit gaat dus automatisch goed.
-}
takeAtLeast :: Int   -- ^ Aantal items om te pakken
            -> a     -- ^ Defaultwaarde
            -> [a]   -- ^ Bronlijst
            -> [a]
takeAtLeast n backup lst = take n lst ++ replicate (n - length lst) backup

-- Done: Schrijf en documenteer de functie context, die met behulp van takeAtLeast de context van de focus-cel in een Automaton teruggeeft. 
-- Niet-gedefinieerde cellen zijn per definitie Dead.
{-
Neem 1 FocusList, bestaande uit cellstates lijsten x en y
1. We gebruiken takeAtLeast om de eerste waarde van y te pakken, dit wordt [Dead] als y == [], dit resultaat voegen we vooraan toe aan de lijst x, en vervolgens gebruiken we takeAtLeast om 3 waarden te pakken.
Return: De context van de focus-cel, levende cellen hebben een waarde, dode cellen zijn Dead.
-}
context :: Automaton -> Context
context (FocusList x y) = takeAtLeast 3 Dead (takeAtLeast 1 Dead y ++ x)


-- Done: Schrijf en documenteer de functie expand die een Automaton uitbreidt met een dode cel aan beide uiteindes. 
-- We doen voor deze simulatie de aanname dat de "known universe" iedere ronde met 1 uitbreidt naar zowel links als rechts.
{-
Neem 1 Automaton FocusList x y
Breidt de automaton toe door aan x en y een Dead cell toe te voegen.
Return: Uitgebreide automaton.
-}
expand :: Automaton -> Automaton
expand (FocusList x y) = FocusList (x++[Dead]) (y++[Dead])

-- Todo: Vul de functie rule30 aan met de andere 7 gevallen. 
-- Voor bonuspunten: doe dit in zo min mogelijk regels code. De underscore _ is je vriend.
rule30 :: Rule
rule30 [Dead, Dead, Dead] = Dead
rule30 [Alive, Dead, Dead] = Alive
rule30 [Alive, _, _] = Dead
rule30 [Dead, _, _] = Alive

-- Je kan je rule-30 functie in GHCi (voer `stack ghci` uit) testen met het volgende commando:
-- putStrLn . showPyramid . iterateRule rule30 15 $ start
-- (Lees sectie 3.5 voor uitleg over deze functies - en hoe het afdrukken nou precies werkt!)

-- De verwachte uitvoer is dan:
{-             ▓
              ▓▓▓
             ▓▓░░▓
            ▓▓░▓▓▓▓
           ▓▓░░▓░░░▓
          ▓▓░▓▓▓▓░▓▓▓
         ▓▓░░▓░░░░▓░░▓
        ▓▓░▓▓▓▓░░▓▓▓▓▓▓
       ▓▓░░▓░░░▓▓▓░░░░░▓
      ▓▓░▓▓▓▓░▓▓░░▓░░░▓▓▓
     ▓▓░░▓░░░░▓░▓▓▓▓░▓▓░░▓
    ▓▓░▓▓▓▓░░▓▓░▓░░░░▓░▓▓▓▓
   ▓▓░░▓░░░▓▓▓░░▓▓░░▓▓░▓░░░▓
  ▓▓░▓▓▓▓░▓▓░░▓▓▓░▓▓▓░░▓▓░▓▓▓
 ▓▓░░▓░░░░▓░▓▓▓░░░▓░░▓▓▓░░▓░░▓
▓▓░▓▓▓▓░░▓▓░▓░░▓░▓▓▓▓▓░░▓▓▓▓▓▓▓ -}


-- ..:: Sectie 3.5: Het herhaaldelijk uitvoeren en tonen van een cellulair automaton ::..
-- In deze sectie hoef je niets aan te passen, maar je moet deze wel even doornemen voor de volgende opgaven.

-- Een reeks van Automaton-states achtereen noemen we een TimeSeries. Effectief dus gewoon een lijst van Automatons over tijd.
type TimeSeries = [Automaton]

-- De functie iterateRule voert een regel n keer uit, gegeven een starttoestand (Automaton). 
-- Het resultaat is een reeks van Automaton-states.
iterateRule :: Rule          -- ^ The rule to apply
            -> Int           -- ^ How many times to apply the rule
            -> Automaton     -- ^ The initial state
            -> TimeSeries
iterateRule r 0 s = [s]
iterateRule r n s = s : iterateRule r (pred n) (fromList $ applyRule $ leftMost $ expand s)
  where applyRule :: Automaton -> Context
        applyRule (FocusList [] bw) = []
        applyRule z = r (context z) : applyRule (goRight z)

-- De functie showPyramid zet een reeks van Automaton-states om in een String die kan worden afgedrukt.
showPyramid :: TimeSeries -> String
showPyramid zs = unlines $ zipWith showFocusList zs $ reverse [0..div (pred w) 2]
  where w = length $ toList $ last zs :: Int
        showFocusList :: Automaton -> Int -> String
        showFocusList z p = replicate p ' ' <> concatMap showCell (toList z)
        showCell :: Cell -> String
        showCell Dead  = "░"
        showCell Alive = "▓"


-- ..:: Sectie 4: Cellulaire automata voor alle mogelijke regels ::..

-- Er bestaan 256 regels, die we niet allemaal met de hand gaan uitprogrammeren op bovenstaande manier. 
-- Zoals op de voorgenoemde pagina te zien is, heeft het nummer te maken met binaire codering. 
-- De toestand van een cel hangt af van de toestand van 3 cellen in de vorige ronde: de cel zelf en diens beide buren (de context). 
-- Afhankelijk van het nummer dat een regel heeft, mapt iedere combinatie naar een levende of dode cel.

-- TODO: Definieer de constante `inputs` die alle 8 mogelijke contexts weergeeft: [Alive,Alive,Alive], [Alive,Alive,Dead], etc.
-- Je mag dit met de hand uitschrijven, maar voor meer punten kun je ook een lijst-comprehensie of andere slimme functie verzinnen.
{-
Met list comprehension kan je een 1 of meerdere lijsten van waardens laten combineren totdat alle unieke combinaties zijn gevonden.
In dit geval, zoekt het alle mogelijke combinaties voor 3 rijen, i,j en k, en kiest het uit twee waarden, Alive of Dead. Het resultaat is een lijst van alle 8 unieke combinaties.
-}
inputs :: [Context]
inputs = [[i,j,k] | i <- [Alive,Dead],
                    j <- [Alive,Dead],
                    k <- [Alive,Dead] ]

-- Deze helperfunctie evalueert met de functie (p) de waarde (x); als dit True teruggeeft, is het resultaat Just x, anders Nothing. 
guard :: (a -> Bool) -> a -> Maybe a
guard p x | p x = Just x
          | otherwise = Nothing

-- TODO: Voorzie de functie `binary` van uitgebreid commentaar.
-- Leg in dit commentaar uit: 
-- 1) Wat de functie precies doet;
-- 2) Stap voor stap, hoe de functie dat doel bereikt.
-- Tips: - Zoek de definitie van `unfoldr` op met Hoogle. 
--       - `toEnum` converteert een Int naar een ander type, in dit geval 0 -> False en 1 -> True voor Bool. 
{-
1. De functie vertaalt een integer waarde tussen 0 en 255 naar binary.
2) 
-}
binary :: Int -> [Bool]
binary = map toEnum . reverse . take 8 . (++ repeat 0)
       . unfoldr (guard (/= (0,0)) . swap . flip divMod 2)

-- Done: Schrijf en documenteer de functie mask, die gegeven een lijst Booleans en een lijst elementen alleen de elementen laat staan 
-- die (qua positie) overeenkomen met een True.
-- Je kunt hiervoor zipWith en Maybe gebruiken (check `catMaybes` in Data.Maybe) of de recursie met de hand uitvoeren.
{-
1. Neem een lijst booleans, x en een lijst met waarden, y.
2. Evalueer de lijsten met zipWith, voor elke True pakken we de Just value, en anders vervangen we de value met Nothing.
catMaybes pruned daarna alle Nothing's, met een masked lijst als resultaat.
-}
mask :: [Bool] -> [a] -> [a]
mask x y = catMaybes $ zipWith(\x y -> if x then Just y else Nothing) x y

-- Done: Schrijf en documenteer de functie rule, die elk getal kan omzetten naar de bijbehorende regel. 
-- De Int staat hierbij voor het nummer van de regel; de Context `input` is waarnaar je kijkt om te zien of het resultaat
-- met de gevraagde regel Dead or Alive is. 
-- Tips: - Denk eraan dat het type Rule een shorthand is voor een functie-type, dus dat je met 2 argumenten te maken hebt. 
--       - Definieer met `where` een subset van `inputs` die tot een levende danwel dode cel leiden.
{-|Allows finding the corresponding rule for every valid integer, 0-255
1. We get in, int i, and evaluate it against a Cell n
2. We convert the given integer into a binary using binary i
3. We then use mask with binary and inputs to find the list of all cell combinations that will return alive for this integer
4. We evaluate if n is in the set created at 3.
5. If it is in the set, we return Alive, if it is not in the set, we return Dead

__Examples:__
@
rule 18 [Alive,Alive,Alive] = Dead
@
-}
rule :: Int -> Rule
rule i n 
  | n `elem` Lib.mask (binary i) inputs = Alive
  | otherwise = Dead

-- map (\y -> Dead) (Lib.mask (binary 30) inputs)
-- type Rule = Context -> Cell [Dead, Dead, Alive] for example*
-- type Context = [Cell]
-- data Cell = Alive | Dead deriving (Show, Eq)

{- Je kunt je rule-functie in GHCi testen met variaties op het volgende commando:

   putStrLn . showPyramid . iterateRule (rule 18) 15 $ start

                  ▓
                 ▓░▓
                ▓░░░▓
               ▓░▓░▓░▓
              ▓░░░░░░░▓
             ▓░▓░░░░░▓░▓
            ▓░░░▓░░░▓░░░▓
           ▓░▓░▓░▓░▓░▓░▓░▓
          ▓░░░░░░░░░░░░░░░▓
         ▓░▓░░░░░░░░░░░░░▓░▓
        ▓░░░▓░░░░░░░░░░░▓░░░▓
       ▓░▓░▓░▓░░░░░░░░░▓░▓░▓░▓
      ▓░░░░░░░▓░░░░░░░▓░░░░░░░▓
     ▓░▓░░░░░▓░▓░░░░░▓░▓░░░░░▓░▓
    ▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓
   ▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓

   Als het goed is zal `stack run` nu ook werken met de optie (d) uit het menu; 
   experimenteer met verschillende parameters en zie of dit werkt.
-}
